from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.orm import relationship

from database import Base
from sqlalchemy.types import Boolean, Integer, Date, VARCHAR


class Role(Base):

    __tablename__ = "roles"
    id = Column(Integer, primary_key=True)
    role_name = Column(String, unique=True)


class User(Base):

    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    user_name = Column(VARCHAR, unique=True)
    first_name = Column(String)
    last_name = Column(String)
    password1 = Column(String)
    email = Column(String)
    contact = Column(String)
    role_id = Column(Integer, ForeignKey('roles.id', ondelete="CASCADE", onupdate="CASCADE"))


class Specialist(Base):
    __tablename__ = "specialist"
    id = Column(Integer, primary_key=True)
    specialist = Column(String, unique=True)


class Appointment(Base):

    __tablename__ = "appointment"
    id = Column(Integer, primary_key=True)
    patient_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE", onupdate="CASCADE"))
    disease = Column(String)
    app_date = Column(Date)
    doctor_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE", onupdate="CASCADE"))
    is_confirm = Column(Boolean)
    is_treated = Column(Boolean)


class Doctor_Specialist(Base):

    __tablename__ = 'doctor_specialist'
    id = Column(Integer, primary_key=True)
    doctor_id = Column(Integer, ForeignKey('users.id', ondelete="CASCADE", onupdate="CASCADE"), unique=True)
    specialist_id = Column(Integer, ForeignKey('specialist.id', ondelete="CASCADE", onupdate="CASCADE"))

