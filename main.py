from fastapi import FastAPI
from models import Base
from database import engine

from router import (register, login, add_doctor, profile, update_profile, remove_doctor, update_doctor,
                    patient_book_appointment, receptionist_book_appointment, patient_see_appointment,
                    receptionist_see_patient_appointment, receptionist_see_doctor_appointment, see_pending_appointment,
                    confirm_appointments, doctor_see_aapointments, doctor_treat, list_of_doctor,
                    appointment_history, update_appointment, add_patient, add_specialist, add_roles)

app = FastAPI()

"""this line will create a table in our database. 
   Here metadata will read all the models and create table"""
Base.metadata.create_all(bind=engine)

app.include_router(register.router)
app.include_router(login.router)
app.include_router(add_doctor.router)
app.include_router(profile.router)
app.include_router(update_profile.router)
app.include_router(remove_doctor.router)
app.include_router(update_doctor.router)
app.include_router(patient_book_appointment.router)
app.include_router(receptionist_book_appointment.router)
app.include_router(patient_see_appointment.router)
app.include_router(receptionist_see_patient_appointment.router)
app.include_router(receptionist_see_doctor_appointment.router)
app.include_router(see_pending_appointment.router)
app.include_router(confirm_appointments.router)
app.include_router(doctor_see_aapointments.router)
app.include_router(doctor_treat.router)
app.include_router(list_of_doctor.router)
app.include_router(appointment_history.router)
app.include_router(update_appointment.router)
app.include_router(add_patient.router)
app.include_router(add_specialist.router)
app.include_router(add_roles.router)
