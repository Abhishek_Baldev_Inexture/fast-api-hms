from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

import database
import models
import schemas
import token_2
import validation
from hashing import Hash

router = APIRouter()

get_db = database.get_db


@router.post('/login')
def login(request: schemas.Login, db: Session = Depends(get_db)):
    """
    This function is used for login of user
    :param request: This will take username and password from user
    :param db: This is used to access the database
    :return: This will return the JWT after successful login
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    username = request.user_name
    password = request.password

    user = db.query(models.User).filter(models.User.user_name == username).first()

    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User does not exit")

    if password != user.password1:
        """ This try is used because sometimes we manually store data in database it does not have hashed password and
             Hash.verify() will throw value error because it need hashed password as first argument"""
        try:
            if not Hash.verify(user.password1, password):
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Incorrect password")
        except ValueError:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Incorrect password")

    access_token = token_2.create_access_token(data={"sub": user.user_name})
    return {"Jwt-token": access_token}
