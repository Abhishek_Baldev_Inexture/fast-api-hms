from datetime import datetime, date

from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from query import is_patient, doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.post('/patient/book_appointment')
def book_appointment(request: schemas.Book_appointment, db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to book appointment. This feature is Authorized by patient
    :param request: This will take the detail for booking appointment
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    patient = is_patient(db, current_user)
    role = doctor_role_id(db)

    doctor = db.query(models.User).filter(models.User.user_name == request.doctor_user_name,
                                          models.User.role_id == role.id).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No Doctor found with that name")


    validate_date = validation.validate_date_formate(request.app_date)
    if validate_date is not True:
        raise validate_date

    date_obj = datetime.strptime(request.app_date, '%d/%m/%Y').date()

    today_date = date.today()
    if date_obj < today_date:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter a correct date")

    data = db.query(models.Appointment).filter(models.Appointment.doctor_id == doctor.id,
                                               models.Appointment.app_date == date_obj).all()
    if len(data) >= 4:
        raise Exception("Can't Book appointment, Please try another date")

    new_appointment = models.Appointment(patient_id=patient.id, doctor_id=doctor.id, disease=request.disease,
                                         app_date=date_obj, is_treated=False, is_confirm=False)

    db.add(new_appointment)
    db.commit()

    return "Your request for appointment has been send....!!!"
