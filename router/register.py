from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

import database
import models
import schemas
import validation
from hashing import Hash
from query import patient_role_id

router = APIRouter()

get_db = database.get_db


@router.post('/register')
def register(request: schemas.Profile, db: Session = Depends(get_db)):
    """
    This function is used the register the new patient and store information in database
    :param request: This is used to get the all detail from front-end
    :param db: This is used to access the database
    :return: This function return nothing
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    username = request.user_name
    firstname = request.first_name
    lastname = request.last_name
    password = request.password
    email = request.email
    contact = request.contact

    patient_id = patient_role_id(db)

    db_data = db.query(models.User).filter(models.User.user_name == username).first()
    if db_data:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"User with name already exit, please try another name")

    validated_password = validation.validate_password(password)
    validated_email = validation.validate_email(email)
    validated_contact = validation.validate_contact(contact)

    # this has a hashed password
    hashed_password = Hash.bcrypt(validated_password)

    new_patient = models.User(user_name=username, first_name=firstname, last_name=lastname, role_id=patient_id.id,
                              password1=hashed_password, email=validated_email, contact=validated_contact)
    db.add(new_patient)
    db.commit()
    return 'Register Successfully'
