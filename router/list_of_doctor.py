from fastapi import APIRouter, Depends
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models

router = APIRouter()

get_db = database.get_db


@router.get('/doctor_list')
def doctor_list(db: Session = Depends(get_db)):
    """
    This function is used to see the list of doctor available
    :param db: This is used to access the database
    :return: This will return the json data of all the doctor
    """
    role_id = db.query(models.Role).filter(models.Role.role_name == "doctor").first()
    doctor = db.query(models.User).filter(models.User.role_id == role_id.id).all()

    return_data = []
    for i in doctor:
        relation = db.query(models.Doctor_Specialist).filter(models.Doctor_Specialist.doctor_id == i.id).first()
        spec = db.query(models.Specialist).filter(models.Specialist.id == relation.specialist_id).first()

        return_data.append({"first_name": i.first_name, "last_name": i.last_name, "specialist": spec.specialist})

    return jsonable_encoder(return_data)
