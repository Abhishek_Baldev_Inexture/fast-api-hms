from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from hashing import Hash
from query import is_receptionist, patient_role_id

router = APIRouter()

get_db = database.get_db


@router.post('/add_patient')
def add_patient(request: schemas.Profile, db: Session = Depends(get_db),
                current_user: schemas.User = Depends(oauth2.get_current_user)):

    value = validation.validate_fields(request)
    if value is not True:
        raise value

    is_receptionist(db, current_user)

    role_id = patient_role_id(db)
    if not role_id:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No role name patient found")

    patient = db.query(models.User).filter(models.User.user_name == request.user_name).first()
    if patient:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="User name already taken")

    validated_email = validation.validate_email(request.email)
    validated_contact = validation.validate_contact(request.contact)

    validated_password = validation.validate_password(request.password)
    hashed_password = Hash.bcrypt(validated_password)

    new_patient = models.User(first_name=request.first_name, user_name=request.user_name,
                              password1=hashed_password, email=validated_email, last_name=request.last_name,
                              contact=validated_contact, role_id=role_id.id)

    db.add(new_patient)
    db.commit()

    return "Patient added successfully"
