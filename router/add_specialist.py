from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session

import database
import models
import schemas
import oauth2
from query import is_receptionist

router = APIRouter()

get_db = database.get_db


@router.post('/add_specialist')
def add_specialist(request: schemas.Specialist, db: Session = Depends(get_db),
                   current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to add the speciality by receptionist
    :param request: This parameter takes speciality from user
    :param db: This is used to get the access of database
    :param current_user: This is used to get currently logged in user
    :return: This function return nothing
    """
    for i in request:
        if i[1] == '' or i[1] is None:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Field '{}' should not be empty".format(i[0]))

    is_receptionist(db, current_user)

    spec_exit = db.query(models.Specialist).filter(models.Specialist.specialist == request.specialist).first()
    if spec_exit:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="Speciality already exit")

    add_new_specialist = models.Specialist(specialist=request.specialist)

    db.add(add_new_specialist)
    db.commit()
    return "Specialist added successfully"
