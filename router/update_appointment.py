from datetime import datetime, date

from fastapi import APIRouter, Depends, status, HTTPException
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from query import is_receptionist, doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.patch('/update_appointment/{app_id}')
def update_appointment(app_id: int, request: schemas.Update_appointment, db: Session = Depends(get_db),
                       current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to update a specific appointment
    :param app_id: This is used to get the id of that appointment
    :param request: This is used to get the detail that we need to update
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """
    for i in request:
        if i[1] == '':
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Field '{}' should not be empty".format(i[0]))

    is_receptionist(db, current_user)

    role_id = doctor_role_id(db)

    appointment = db.query(models.Appointment).filter(models.Appointment.id == app_id).first()
    if not appointment:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No appointment for id found")
    if appointment.is_treated is True:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail="Please enter another id")


    if request.app_date is not None:
        validated_date = validation.validate_date_formate(request.app_date)
        if validated_date is not True:
            raise validated_date

        date_obj = datetime.strptime(request.app_date, '%d/%m/%Y').date()

        if date_obj < date.today():
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Please Enter a correct date")
        appointment.app_date = date_obj



    if request.doctor is not None:
        doctor = db.query(models.User).filter(models.User.user_name == request.doctor,
                                              models.User.role_id == role_id.id).first()
        if not doctor:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="No Doctor found, Please enter a correct name")

        appointment.doctor_id = doctor.id

        appointment.is_confirm = False

    db.commit()

    return "Appointment updated successfully"
