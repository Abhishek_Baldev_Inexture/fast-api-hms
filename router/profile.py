from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

import database, schemas, oauth2, models

router = APIRouter()

get_db = database.get_db


@router.get('/profile')
def view_profile(db: Session = Depends(get_db),
                 current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to see the profile information of currently logged-in user
    :param db: This is used access the database
    :param current_user: This is used to get the currently logged-in user
    :return: This function return the json data of profile information
    """
    user = db.query(models.User).filter(models.User.user_name == current_user.username).first()
    if not user:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                             detail="No User found")

    spec = db.query(models.Role).filter(models.Role.id == user.role_id).first()

    # This data is used to send output of current user
    data = db.query(models.User.user_name, models.User.first_name, models.User.last_name, models.User.password1,
                    models.User.email, models.User.contact).\
        filter(models.User.user_name == current_user.username).first()

    json_data = jsonable_encoder(data)
    json_data.update({"role": spec.role_name})

    return json_data
