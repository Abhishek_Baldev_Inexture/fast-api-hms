from datetime import datetime, date

from fastapi import APIRouter, HTTPException, Depends, status
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from hashing import Hash
from query import is_receptionist

router = APIRouter()

get_db = database.get_db


@router.post('/receptionist/book_appointment')
def book_appointment(request: schemas.Receptionist_book_appointment, db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to book appointment. This feature is Authorized by Receptionist only
    :param request: This is used to get information of patient to book appointment
    :param db: This is used to access the database
    :param current_user: This is used to get the currently logged-in user
    :return: This function return nothing
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    is_receptionist(db, current_user)

    patient_id = db.query(models.Role).filter(models.Role.role_name == "patient").first()
    doctor_id = db.query(models.Role).filter(models.Role.role_name == "doctor").first()

    patient = db.query(models.User).filter(models.User.user_name == request.patient_user_name,
                                           models.User.role_id == patient_id.id).first()
    if not patient:
        hashed_password = Hash.bcrypt('@Jay1234')
        patient = models.User(user_name=request.patient_user_name, password1=hashed_password, role_id=patient_id.id)
        db.add(patient)
        db.commit()

    doctor = db.query(models.User).filter(models.User.user_name == request.doctor_user_name,
                                          models.User.role_id == doctor_id.id).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No doctor name found")

    validated_date = validation.validate_date_formate(request.app_date)
    if validated_date is not True:
        raise validated_date

    date_obj = datetime.strptime(request.app_date, '%d/%m/%Y').date()

    today_date = date.today()
    if date_obj < today_date:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter a valid date")

    data = db.query(models.Appointment).\
        filter(models.Appointment.doctor_id == doctor.id,
               models.Appointment.app_date == date_obj).all()
    if len(data) >= 4:
        raise Exception("Can't Book appointment, Please try another date")

    new_appointment = models.Appointment(patient_id=patient.id, doctor_id=doctor.id, disease=request.disease,
                                         app_date=date_obj, is_confirm=True, is_treated=False)
    db.add(new_appointment)
    db.commit()

    return "Your appointment is booked...!!!"
