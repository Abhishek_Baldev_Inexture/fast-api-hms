from fastapi import APIRouter, Depends, status, HTTPException, Query
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist, patient_role_id

router = APIRouter()

get_db = database.get_db


@router.get('/receptionist/patient_appointments/')
def see_appointments(patient_name: str = Query(default=None), db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used see the appointments of specific patient
    :param patient_name: It will take patient name in url to see the appointment
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return the json data of all appointment of that specific patient
    """
    if patient_name == '' or patient_name is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter the patient name")

    is_receptionist(db, current_user)
    role_id = patient_role_id(db)

    patient = db.query(models.User).filter(models.User.user_name == patient_name,
                                           models.User.role_id == role_id.id).first()
    if not patient:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No patient found with such name")

    appointment = db.query(models.Appointment.disease, models.Appointment.app_date, models.Appointment.doctor_id,
                           models.Appointment.is_confirm, models.Appointment.is_treated). \
        filter(models.Appointment.patient_id == patient.id).all()

    if not appointment:
        return "No appointment Found for this patient"


    doctor_ids = []
    for i in appointment:
        doctor_ids.append(i.doctor_id)

    doctor = db.query(models.User.user_name).filter(models.User.id.in_(doctor_ids)).all()

    json_data = jsonable_encoder(appointment)
    for i in range(len(json_data)):
        patient_user = {"doctor_name": doctor[i].user_name}
        json_data[i].update(patient_user)

    return json_data
