from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_patient

router = APIRouter()

get_db = database.get_db


@router.get('/patient/see_appointment')
def see_appointment(db: Session = Depends(get_db),
                    current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to see the appointment of currently logged-in patient
    :param db: this is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return the json data of the appointment details
    """

    patient = is_patient(db, current_user)

    appointment = db.query(models.Appointment.app_date, models.Appointment.disease, models.Appointment.doctor_id,
                           models.Appointment.is_confirm, models.Appointment.is_treated).\
        filter(models.Appointment.patient_id == patient.id).all()

    if not appointment:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Sorry..!!! You haven't book any appointment")

    json_data = jsonable_encoder(appointment)

    for i in range(len(appointment)):
        doctor = db.query(models.User).filter(models.User.id == appointment[i].doctor_id).first()
        json_data[i].update({"doctor_name": doctor.user_name})

    return json_data
