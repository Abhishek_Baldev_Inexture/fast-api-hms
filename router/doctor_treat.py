from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from datetime import date

import database, schemas, oauth2, models
from query import is_doctor
router = APIRouter()

get_db = database.get_db


@router.put('/treat_patient/{app_id}')
def treat_patient(app_id: int, db: Session = Depends(get_db),
                  current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to treat people. This feature is Authorized by doctor only
    :param app_id: This will take the id of patient that is to be treated
    :param db: This is used to access the database
    :param current_user: This is used to get the currently logged-in user
    :return: This function return nothing
    """

    user = is_doctor(db, current_user)
    date1 = date.today()

    appointment = db.query(models.Appointment).filter(models.Appointment.id == app_id).first()
    if not appointment:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Invalid appointment id")

    doctor = db.query(models.User).filter(models.User.id == appointment.doctor_id).first()

    if user.user_name != doctor.user_name:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="You can't treat this patient")
    if appointment.is_confirm is False:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Appointment is not confirmed yet")
    if appointment.app_date != date1:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="This patient has not appointments for today")

    db.query(models.Appointment).filter(models.Appointment.id == app_id).\
        update({models.Appointment.is_treated: True})

    db.commit()

    return "Patient has been treated"
