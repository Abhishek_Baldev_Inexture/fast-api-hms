from datetime import date

from fastapi import APIRouter, Depends, HTTPException, status, Query
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist, patient_role_id

router = APIRouter()

get_db = database.get_db


@router.get('/appointment_history/')
def appointment_history(db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user),
                        patient_name: str = Query(default=None)):
    """
    This function is used to see the appointment history. This feature is Authorized by Receptionist only
    :param patient_name:
    :param db: This is to get access of database
    :param current_user: This is used to get currently logged-in user
    :return: This function return the json data of appointment history
    """

    if patient_name == '' or patient_name is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter the patient name")

    is_receptionist(db, current_user)
    role_id = patient_role_id(db)
    if not role_id:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No role name patient found")

    today_date = date.today()

    patient = db.query(models.User).filter(models.User.user_name == patient_name,
                                           models.User.role_id == role_id.id).first()
    if not patient:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Please Enter a correct username")

    appointments = db.query(models.Appointment.app_date, models.Appointment.doctor_id, models.Appointment.is_confirm,
                            models.Appointment.is_treated).\
        filter(models.Appointment.patient_id == patient.id, models.Appointment.app_date < today_date).all()
    if not appointments:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No appointment history of patient")

    json_data = jsonable_encoder(appointments)

    for i in range(len(appointments)):
        doctor = db.query(models.User).filter(models.User.id == appointments[i].doctor_id).first()
        json_data[i].update({"doctor_name": doctor.user_name})

    return json_data
