from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from datetime import date

import database, schemas, oauth2, models
from query import is_doctor

router = APIRouter()

get_db = database.get_db


@router.get('/doctor/see_appointments')
def see_appointments(db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to see the current and future appointment of currently logged-in doctor
    :param db: This is used to access the database
    :param current_user: This is used to get the currently logged-in doctor.
    :return: This function return json data of appointment.
    """

    doctor = is_doctor(db, current_user)

    date1 = date.today()

    appointments = db.query(models.Appointment.disease, models.Appointment.app_date, models.Appointment.is_treated).\
        filter(models.Appointment.doctor_id == doctor.id,
               models.Appointment.app_date >= date1,
               models.Appointment.is_confirm.is_(True)).all()

    if not appointments:
        return "No further appointments are their"

    json_data = jsonable_encoder(appointments)

    return json_data
