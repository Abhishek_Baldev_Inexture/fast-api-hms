from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

import database
import models
import schemas
import oauth2
import query

router = APIRouter()

get_db = database.get_db


@router.post('/add_role')
def add_role(role: schemas.Add_role, db: Session = Depends(get_db),
             current_user: schemas.User = Depends(oauth2.get_current_user)):

    print(role)
    if role.role_name is None or role.role_name == '':
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please Enter role")

    query.is_receptionist(db, current_user)

    role_exist = db.query(models.Role).filter(models.Role.role_name == role.role_name).first()
    if role_exist:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="Role already exist")

    new_role = models.Role(role_name=role.role_name)

    db.add(new_role)
    db.commit()

    return "Role added successfully"
