from fastapi import APIRouter, Depends, status, HTTPException
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from hashing import Hash

router = APIRouter()

get_db = database.get_db


@router.patch('/update_profile')
async def update_profile(request: schemas.Profile, db: Session = Depends(database.get_db),
                         current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to update the profile details of user
    :param request: This is used to get the new details from user
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """
    for i in request:
        if i[1] == '':
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Field '{}' should not be empty".format(i[0]))

    user = db.query(models.User).filter(models.User.user_name == current_user.username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not Found")

    existing_user = db.query(models.User).filter(models.User.user_name == request.user_name).first()
    if existing_user:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="User name already taken, Please Enter another name")


    if request.user_name is not None:
        user.user_name = request.user_name

    if request.first_name is not None:
        user.first_name = request.first_name

    if request.last_name is not None:
        user.last_name = request.last_name

    if request.password is not None:
        validated_password = validation.validate_password(request.password)
        hashed_password = Hash.bcrypt(validated_password)
        user.password1 = hashed_password

    if request.email is not None:
        validated_email = validation.validate_email(request.email)
        user.email = validated_email

    if request.contact is not None:
        validated_contact = validation.validate_contact(request.contact)
        user.contact = validated_contact

    # else:
    #     raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Enter a valid fields")

    db.commit()

    return "Details Updated successfully"
