from fastapi import APIRouter, Depends, status, HTTPException
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist

router = APIRouter()

get_db = database.get_db


@router.put('/receptionist/confirm_appointments/{app_id}')
def confirm_appointments(app_id: int, db: Session = Depends(get_db),
                         current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to confirm the appointment of patient. This feature is Authorized by receptionist only
    :param app_id: This will take the id of appointment which need to be confirmed
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """

    is_receptionist(db, current_user)

    appointment = db.query(models.Appointment).filter(models.Appointment.id == app_id).first()
    if not appointment:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No appointments with id found")

    if appointment.is_confirm is True:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail="Appointment already confirmed")
    db.query(models.Appointment).filter(models.Appointment.id == app_id).update({models.Appointment.is_confirm: True})

    db.commit()

    return "Appointment is confirm"
