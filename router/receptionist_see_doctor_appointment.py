from fastapi import APIRouter, HTTPException, status, Depends, Query
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist, doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.get('/receptionist/doctor_appointments/')
def see_appointments(doctor_name: str = Query(default=None), db: Session = Depends(get_db),
                     current_user: schemas.User = Depends(oauth2.get_current_user)):

    """
    This function is used see the appointments of specific doctor
    :param doctor_name: It will take doctor as a query parameter
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return the json data of all appointment of that specific doctor
    """
    if doctor_name == '' or doctor_name is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter the doctor name")

    is_receptionist(db, current_user)
    role_id = doctor_role_id(db)

    doctor = db.query(models.User).filter(models.User.user_name == doctor_name,
                                          models.User.role_id == role_id.id).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No Doctor name found, Please Enter a correct name")

    appointment = db.query(models.Appointment.disease, models.Appointment.app_date,
                           models.Appointment.is_confirm, models.Appointment.is_treated,
                           models.Appointment.patient_id).\
        filter(models.Appointment.doctor_id == doctor.id).all()

    if not appointment:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="No appointment Found for this doctor")

    patient_ids = []
    for i in appointment:
        patient_ids.append(i.patient_id)

    patient = db.query(models.User.user_name).filter(models.User.id.in_(patient_ids)).all()

    json_data = jsonable_encoder(appointment)

    for i in range(len(json_data)):
        patient_user = {"patient_name": patient[i].user_name}
        json_data[i].update(patient_user)

    return json_data
