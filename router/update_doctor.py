from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from query import is_receptionist, doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.patch('/update_doctor/{doctor_id}')
async def update_doctor(doctor_id: int, request: schemas.Specialist, db: Session = Depends(get_db),
                        current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to update the doctor information
    :param doctor_id: This is used to get the id to update the doctor
    :param request: This is used to get the details to update
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    is_receptionist(db, current_user)
    role_id = doctor_role_id(db)

    doctor = db.query(models.User).filter(models.User.id == doctor_id,
                                          models.User.role_id == role_id.id).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Please enter a valid id for doctor")

    specialist = db.query(models.Specialist).filter(models.Specialist.specialist == request.specialist).first()
    if not specialist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No Speciality found, Please enter a correct details")

    db.query(models.Doctor_Specialist).filter(models.Doctor_Specialist.doctor_id == doctor.id).\
        update({models.Doctor_Specialist.specialist_id: specialist.id})

    db.commit()

    return "Details Updated successfully"
