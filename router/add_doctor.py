from fastapi import APIRouter, HTTPException, status, Depends
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
import validation
from hashing import Hash
from query import is_receptionist, doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.post('/add_doctor')
def add_doctor(request: schemas.Add_doctor, db: Session = Depends(get_db),
               current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to add doctor. This function is Authorized by Receptionist only.

    :param request: This is used to take details of doctor from front-end
    :param db: This is used to get access of database
    :param current_user: This is used to get the current logged-in user
    :return: This return nothing
    """
    value = validation.validate_fields(request)
    if value is not True:
        raise value

    is_receptionist(db, current_user)

    role_id = doctor_role_id(db)
    if not role_id:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No Role for doctor found")

    username = request.user_name
    firstname = request.first_name
    lastname = request.last_name
    password = request.password
    email = request.email
    contact = request.contact
    specialist = request.specialist

    doctor = db.query(models.User).filter(models.User.user_name == username).first()
    if doctor:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="User name already taken")

    validated_email = validation.validate_email(email)
    validated_contact = validation.validate_contact(contact)

    hashed_password = Hash.bcrypt(password)

    new_doctor = models.User(first_name=firstname, user_name=username, last_name=lastname, password1=hashed_password,
                             email=validated_email, contact=validated_contact, role_id=role_id.id)

    spec = db.query(models.Specialist).filter(models.Specialist.specialist == specialist).first()
    if not spec:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Requested specialization not found")
    db.add(new_doctor)
    db.commit()

    new_mapper = models.Doctor_Specialist(doctor_id=new_doctor.id, specialist_id=spec.id)

    db.add(new_mapper)
    db.commit()

    return "Doctor added successfully"
