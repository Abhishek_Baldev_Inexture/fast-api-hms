from fastapi import APIRouter, Depends, HTTPException, status, Query
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist

router = APIRouter()

get_db = database.get_db


@router.get('/pending_appointments/')
def pending_appointments(doctor_name: str = Query(default=None), db: Session = Depends(get_db),
                         current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to see all the pending request for appointment.
    This feature is Authorized by receptionist only

    :param doctor_name: It will take doctor name as query parameter
    :param db: This is used to access the database
    :param current_user: This is used to get the currently logged-in user
    :return: This function return json data of all the pending appointments
    """
    if doctor_name == '' or doctor_name is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Please enter the doctor name")

    is_receptionist(db, current_user)

    doctor = db.query(models.User).filter(models.User.user_name == doctor_name).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="No doctor found this name")

    """ this query filter is taken from = 
        https://stackoverflow.com/questions/38226607/python-flask-sqlalchemy-query-filter-by-boolean-values-ignore-false
    """
    appointments = db.query(models.Appointment).filter(models.Appointment.doctor_id == doctor.id,
                                                       models.Appointment.is_confirm.is_(False)).all()

    if not appointments:
        return "No pending request"

    json_data = jsonable_encoder(appointments)

    return json_data
