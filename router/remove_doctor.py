from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

import database
import models
import oauth2
import schemas
from query import is_receptionist
from query import doctor_role_id

router = APIRouter()

get_db = database.get_db


@router.delete('/delete_doctor/{doctor_id}')
async def delete_doctor(doctor_id: int, db: Session = Depends(database.get_db),
                        current_user: schemas.User = Depends(oauth2.get_current_user)):
    """
    This function is used to remove the doctor from database. This feature is Authorized by receptionist only
    :param doctor_id: This is used to get the id that specific doctor
    :param db: This is used to access the database
    :param current_user: This is used to get currently logged-in user
    :return: This function return nothing
    """
    is_receptionist(db, current_user)

    role_id = doctor_role_id(db)
    doctor = db.query(models.User).filter(models.User.id == doctor_id,
                                          models.User.role_id == role_id.id).first()
    if not doctor:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Please enter a valid id for doctor")

    db.query(models.User).filter(models.User.id == doctor_id).delete()
    db.query(models.Appointment).filter(models.Appointment.doctor_id == doctor_id).delete()
    db.query(models.Doctor_Specialist).filter(models.Doctor_Specialist.doctor_id == doctor_id).delete()
    db.commit()

    return "Doctor deleted successfully"
