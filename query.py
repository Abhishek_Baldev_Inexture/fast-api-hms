from fastapi import HTTPException, status

import models


def is_receptionist(db, current_user):

    user = db.query(models.User).filter(models.User.user_name == current_user.username).first()

    role = db.query(models.Role).filter(models.Role.id == user.role_id).first()

    if role.role_name != "receptionist":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Service not Authorized")
    return user


def is_patient(db, current_user):

    user = db.query(models.User).filter(models.User.user_name == current_user.username).first()

    role = db.query(models.Role).filter(models.Role.id == user.role_id).first()

    if role.role_name != "patient":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Service not Authorized")
    return user


def is_doctor(db, current_user):

    user = db.query(models.User).filter(models.User.user_name == current_user.username).first()

    role = db.query(models.Role).filter(models.Role.id == user.role_id).first()

    if role.role_name != "doctor":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Service not Authorized")
    return user


def doctor_role_id(db):
    role = db.query(models.Role).filter(models.Role.role_name == "doctor").first()
    return role


def patient_role_id(db):
    role = db.query(models.Role).filter(models.Role.role_name == "patient").first()
    return role


def receptionist_role_id(db):
    role = db.query(models.Role).filter(models.Role.role_name == "receptionist").first()
    return role
