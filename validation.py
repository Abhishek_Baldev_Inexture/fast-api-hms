import datetime

from fastapi import HTTPException, status
import re


def validate_password(password):
    if len(password) < 8:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Length of password should be greater than 8")
    if re.search("[a-z]", password) is None:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Password should contain at-least one lower letter")
    if re.search("[A-Z]", password) is None:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Password should contain at-least one upper letter")
    if re.search("[0-9]", password) is None:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Password should contain at-least one digit")
    else:
        return password


def validate_email(email):
    if re.search(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', email) is None:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Please enter valid E-mail")
    else:
        return email


def validate_contact(contact):
    if len(contact) < 10:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Length of contact should not less than 10")
    if contact.isdigit() is False:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f"Enter proper contact number")
    else:
        return contact


def validate_fields(request):
    for i in request:
        if i[1] is None or i[1] == '':
            return HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                 detail="Field '{}' is require for adding patient".format(i[0]))
    else:
        return True


def validate_date_formate(date):
    date_string = date
    format1 = "%d/%m/%Y"

    try:
        datetime.datetime.strptime(date_string, format1)
        return True
    except ValueError:
        return HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                             detail="This is the incorrect date string format. It should be DD/MM/YYYY")
