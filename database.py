from sqlalchemy import create_engine

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import os
from dotenv import load_dotenv

load_dotenv()

# database_url = 'sqlite:///Shopping_application/E_shop.db's

# engine = create_engine(SQLALCHEMY_DATABASE_URL,connect_args={"check_same_thread": False})


def get_url():
    SQLALCHEMY_DATABASE_URL = f"postgresql://{os.getenv('USER1')}:{os.getenv('PASS')}@{os.getenv('HOST')}:5432/{os.getenv('DB')}"
    return SQLALCHEMY_DATABASE_URL


engine = create_engine(get_url())
SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
