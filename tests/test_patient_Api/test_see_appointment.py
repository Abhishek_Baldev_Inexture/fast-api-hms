from fastapi.testclient import TestClient
from main import app

from ..test_patient_Api import test_login, test_book_appointment

client = TestClient(app)

token = test_login.test_login()


def test_no_appointment():
    response = client.get('/patient/see_appointment', headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Sorry..!!! You haven't book any appointment"}, response.text


def test_without_token():
    response = client.get('/patient/see_appointment')
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_wrong_token():
    response = client.get('/patient/see_appointment', headers={"Authorization": "Bearer sdkjadsjDjwqemME2LK312312M323"})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text


def test_see_appoint():
    test_book_appointment.test_book_appointment()
    response = client.get('/patient/see_appointment', headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text
