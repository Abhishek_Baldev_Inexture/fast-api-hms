import json
from fastapi.testclient import TestClient

from main import app
from ..test_patient_Api import test_register

client = TestClient(app)
test_register.test_register()


def test_login():
    data = {"user_name": "mohit12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code == 200, response.text
    token = response.json()
    return token


def test_wrong_data():
    data = {"user_name": "mohit123", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "User does not exit"}, response.text


def test_wrong_pass():
    data = {"user_name": "mohit12", "password": "@Jay123"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Incorrect password"}, response.text


def test_none():
    data = None
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text


def test_empty():
    # test_register.test_register()
    data = {}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text


def test_missing_fields():
    # test_register.test_register()
    data = {"user_name": "mohit12"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text


def test_missing_values():
    # test_register.test_register()
    data = {"user_name": "mohit12", "password": ""}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text

