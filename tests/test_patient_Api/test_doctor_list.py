import json
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_doctor_list():
    response = client.get('/doctor_list')
    assert response.status_code == 200, response.text
