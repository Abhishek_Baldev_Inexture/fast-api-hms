import json

from fastapi.testclient import TestClient
from sqlalchemy import select

import models
from main import app
from .. import create_database


client = TestClient(app)

stmt = create_database.engine.execute(select(models.Role).where(models.Role.role_name == "patient"))
result = stmt.fetchone()


def test_register():
    create_database.override_get_db()
    data = {"user_name": "mohit12", "first_name": "mohit", "last_name": "patel", "password": "@Jay1234",
            "email": "test@gmail.com", "contact": "9898989898", "role_id": result.id}

    response = client.post('/register', json.dumps(data))
    assert response.status_code == 200, response.text
    assert response.json() == 'Register Successfully', response.text


def test_none():
    # create_database.override_get_db()
    data = None
    response = client.post('/register', json.dumps(data))
    assert response.status_code != 200, response.text


def test_empty():
    # create_database.override_get_db()
    data = {}
    response = client.post('/register', json.dumps(data))
    assert response.status_code != 200, response.text


def test_missing_fields():
    # create_database.override_get_db()
    data = {"user_name": "mohit12", "first_name": "mohit", "last_name": "patel", "password": "@Jay1234",
            "email": "test@gmail.com", "role_id": result.id}

    response = client.post('/register', json.dumps(data))
    assert response.status_code != 200, response.text


def test_missing_values():
    # create_database.override_get_db()
    data = {"user_name": "", "first_name": "mohit", "last_name": "patel", "password": "@Jay1234",
            "email": "test@gmail.com", "contact": "9898989898", "role_id": result.id}

    response = client.post('/register', json.dumps(data))
    assert response.status_code != 200, response.text


def test_duplication():
    data = {"user_name": "mohit12", "first_name": "mohit", "last_name": "patel", "password": "@Jay1234",
            "email": "test@gmail.com", "contact": "9898989898", "role_id": result.id}

    response = client.post('/register', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "User with name already exit, please try another name"}, response.text
