import json
from fastapi.testclient import TestClient
from main import app
from ..test_patient_Api import test_login

client = TestClient(app)

token = test_login.test_login()


def test_update_profile():
    data = {"user_name": "mohit123"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_without_token():
    data = {"user_name": "mohit123"}
    response = client.patch('/update_profile', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_none():
    data = None
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_missing_value():
    data = {"user_name": ""}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text

