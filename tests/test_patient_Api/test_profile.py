from fastapi.testclient import TestClient
from main import app
from tests.test_patient_Api import test_login

client = TestClient(app)

token = test_login.test_login()


def test_profile():
    response = client.get('/profile', headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text
    data = response.json()


def test_without_token():
    response = client.get('/profile')
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_wrong_token():
    response = client.get('/profile', headers={"Authorization": "Bearer 121ijeoqiddkodkaspdasdmasdmasdmasdlm,askdm"})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text

