import json

from fastapi.testclient import TestClient
from main import app
from tests.test_patient_Api import test_login
from ..test_receptoinist_api import test_add_doctor

client = TestClient(app)

token = test_login.test_login()


def test_book_appointment():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "15/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_wrong_doctor():
    # test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "7/06/2022", "doctor_user_name": "khushi123"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No Doctor found with that name"}, response.text


def test_none():
    # token = test_login.test_login()
    # test_add_doctor.test_add_doctor()
    data = None
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_without_token():
    # token = test_login.test_login()
    # test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "1/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_wrong_token():
    data = {"disease": "cavities", "app_date": "1/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer aasjdoaijdoadaDSA;MDaDQWDPOASKDLAS"})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text


def test_missing_value():
    # token = test_login.test_login()
    # test_add_doctor.test_add_doctor()
    data = {"disease": "", "app_date": "1/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_missing_fields():
    # token = test_login.test_login()
    # test_add_doctor.test_add_doctor()
    data = {"app_date": "1/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_wrong_date():
    # token = test_login.test_login()
    # test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "1/01/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter a correct date"}, response.text

