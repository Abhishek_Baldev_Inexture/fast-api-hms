import json
from fastapi.testclient import TestClient
from main import app
from ..test_doctor_Api import test_doct_login

client = TestClient(app)

# test_add_doctor.test_add_doctor()
token = test_doct_login.test_doct_login()


def test_none():
    data = {}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text


def test_invalid_email():
    data = {"email": "test@.com"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter valid E-mail"}, response.text


def test_invalid_password():
    data = {"password": "12334"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text


def test_invalid_number():
    data = {"contact": "982173"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text


def test_update_name():
    data = {"user_name": "khushi123"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text
    assert response.json() == "Details Updated successfully", response.text
