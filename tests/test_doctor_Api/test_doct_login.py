import json
from fastapi.testclient import TestClient
from main import app
from ..test_patient_Api import test_register
from ..test_receptoinist_api import test_add_doctor, test_recep_login

client = TestClient(app)

test_register.test_register()


def test_no_doctor():
    data = {"user_name": "khushi12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "User does not exit"}, response.text


def test_doct_login():
    test_add_doctor.test_add_doctor()
    data = {"user_name": "khushi12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code == 200, response.text
    token = response.json()
    return token


def test_none():
    data = None
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text


def test_missing_value():
    data = {"user_name": "khushi12", "password": ""}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text


def test_wrong_pass():
    data = {"user_name": "khushi12", "password": "@Jay123"}
    response = client.post('/login', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Incorrect password"}, response.text

