import json
from fastapi.testclient import TestClient
from main import app
from ..test_patient_Api import test_login
from ..test_receptoinist_api import test_recep_book_aapointment, test_recep_login

client = TestClient(app)


def login():
    data = {"user_name": "khushi12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    token = response.json()
    return token


def patient_book_appoint():
    token = test_login.test_login()
    data = {"disease": "teeth-ache", "app_date": "15/06/2022", "doctor_user_name": "khushi12"}
    response = client.post('/patient/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def add_doctor():
    token = test_recep_login.test_recep_login()
    data = {"user_name": "vasu12", "first_name": "vasu", "last_name": "patel", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text


def doct_2_login():
    add_doctor()
    data = {"user_name": "vasu12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    token = response.json()
    return token


def test_treat():
    test_recep_book_aapointment.test_recep_book_appointment()
    token = login()
    app_id = 1
    response = client.put(f'/treat_patient/{app_id}', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text


def test_not_confirm():
    patient_book_appoint()
    token = login()
    app_id = 2
    response = client.put(f'/treat_patient/{app_id}', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Appointment is not confirmed yet"}, response.text


def test_wrong_doctor():
    token = doct_2_login()
    app_id = 2
    response = client.put(f'/treat_patient/{app_id}', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "You can't treat this patient"}, response.text


def test_wrong_id():
    token = login()
    app_id = 3
    response = client.put(f'/treat_patient/{app_id}', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Invalid appointment id"}, response.text
