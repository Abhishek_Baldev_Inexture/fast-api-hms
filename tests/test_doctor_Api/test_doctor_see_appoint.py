import json
from fastapi.testclient import TestClient
from sqlalchemy import delete

import models
from main import app
from ..test_receptoinist_api import test_recep_book_aapointment
from ..test_patient_Api import test_register
from ..create_database import engine

client = TestClient(app)
test_register.test_register()


def login_doctor():
    data = {"user_name": "khushi12", "password": "@Jay1234"}
    response = client.post('/login', json.dumps(data))
    token = response.json()
    return token


def test_see_appoint():
    test_recep_book_aapointment.test_recep_book_appointment()
    token = login_doctor()
    response = client.get('/doctor/see_appointments', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text


def test_no_appoint():
    stmt = delete(models.Appointment)
    engine.execute(stmt)
    token = login_doctor()
    response = client.get('/doctor/see_appointments', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text
    assert response.json() == "No further appointments are their", response.text


def test_wrong_token():
    response = client.get('/doctor/see_appointments', headers={"Authorization": "Bearer 1312eDSAJANXAXKzxlzXZXCZx"})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text
