import json
from fastapi.testclient import TestClient
from main import app
from tests import create_database
from ..test_doctor_Api import test_doct_login

client = TestClient(app)

create_database.override_get_db()
token = test_doct_login.test_doct_login()


def test_profile():
    response = client.get('/profile', headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text
    data = response.json()


def test_without_token():
    response = client.get('/profile')
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_wrong_token():
    response = client.get('/profile', headers={"Authorization": "Bearer 121ijeoqiddkodkaspdasdmasdmasdmasdlmaskdm"})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text
