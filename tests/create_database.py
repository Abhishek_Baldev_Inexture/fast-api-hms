from sqlalchemy import create_engine, insert, select
from sqlalchemy.orm import sessionmaker

import models
from database import get_db

from main import app


database_url = 'sqlite:///../hms.db'
engine = create_engine(database_url, connect_args={"check_same_thread": False})
TestingSessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)


models.Base.metadata.drop_all(bind=engine)
print("hiii")
models.Base.metadata.create_all(bind=engine)


# this thing I got from https://stackoverflow.com/questions/18708050/bulk-insert-list-values-with-sqlalchemy-core
engine.execute(insert(models.Role), [{'role_name': 'receptionist'},
                                     {'role_name': 'doctor'},
                                     {'role_name': 'patient'}])

stmt = engine.execute(select(models.Role).where(models.Role.role_name == "receptionist"))
result = stmt.fetchone()

engine.execute(insert(models.Specialist), [{'specialist': 'dentist'},
                                           {'specialist': 'orthopaedic'}])

receptionist = insert(models.User).values(user_name="riya12", first_name="riya", last_name="patel", role_id=result.id,
                                          password1="@Jay1234", email="test@gmail.com", contact="9898989898")
engine.execute(receptionist)
# engine.commit() because engine object does not require commit to reflect data into database


def override_get_db():
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db
