import json
from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login
from ..test_patient_Api import test_register

client = TestClient(app)
test_register.test_register()
token = test_recep_login.test_recep_login()


def test_add_specialist():
    data = {"specialist": "pediatrician"}
    response = client.post('/add_specialist', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code == 200, response.text
    assert response.json() == "Specialist added successfully", response.text


def test_already_exist():
    data = {"specialist": "pediatrician"}
    response = client.post('/add_specialist', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Speciality already exit"}, response.text


def test_empty():
    data = {"specialist": ""}
    response = client.post('/add_specialist', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code != 200, response.text


def test_none():
    data = {}
    response = client.post('/add_specialist', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code != 200, response.text
