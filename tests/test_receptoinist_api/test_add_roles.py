import json
from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login


client = TestClient(app)
token = test_recep_login.test_recep_login()


def test_add_role():
    data = {"role_name": "admin"}
    response = client.post('/add_role', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code == 200, response.text


def test_role_exit():
    data = {"role_name": "admin"}
    response = client.post('/add_role', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Role already exist"}, response.text


def test_empty():
    data = {}
    response = client.post('/add_role', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please Enter role"}, response.text


def test_without_token():
    data = {"role_name": "admin"}
    response = client.post('/add_role', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text
