import json
from fastapi.testclient import TestClient
from sqlalchemy import delete

import models
from main import app
from ..test_receptoinist_api import test_recep_login
from ..create_database import engine

client = TestClient(app)
token = test_recep_login.test_recep_login()


def test_add_doctor():
    data = {"user_name": "khushi12", "first_name": "khushi", "last_name": "patel", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text


def test_none():
    data = None
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_empty():
    data = {}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_missing_fields():
    data = {"user_name": "khushi12", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_missing_values():
    data = {"user_name": "", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_wrong_speciality():
    data = {"user_name": "khushi123", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "pediatrician"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Requested specialization not found"}, response.text


def test_no_doctor():
    stmt = delete(models.Role).where(models.Role.role_name == "doctor")
    engine.execute(stmt)

    data = {"user_name": "khushi123", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No Role for doctor found"}, response.text
