import json
from fastapi.testclient import TestClient
from main import app
from ..test_patient_Api import test_register
from ..test_receptoinist_api import test_add_doctor, test_recep_login

client = TestClient(app)
test_register.test_register()
token = test_recep_login.test_recep_login()


def test_recep_book_appointment():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "15/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_none():
    # test_add_doctor.test_add_doctor()
    data = None
    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_empty():
    data = {}
    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_without_token():
    data = {"disease": "cavities", "app_date": "8/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}


def test_missing_fields():
    data = {"disease": "cavities", "app_date": "8/06/2022", "doctor_user_name": "khushi12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_missing_values():
    data = {"disease": "", "app_date": "8/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_wrong_date():
    data = {"disease": "cavities", "app_date": "1/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter a valid date"}, response.text


def test_wrong_doctor():
    data = {"disease": "cavities", "app_date": "8/06/2022", "doctor_user_name": "khushi123",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No doctor name found"}, response.text


def test_wrong_patient():
    """ When you will enter wrong patient name that does not exit in user table, recep has right it first creates
        a patient, store it in user table, and then it will book appointment for that"""

    data = {"disease": "cavities", "app_date": "15/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "abhi12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text
