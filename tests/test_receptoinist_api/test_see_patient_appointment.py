import json
from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login, test_add_doctor


client = TestClient(app)

token = test_recep_login.test_recep_login()


def book_appoint():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "16/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})


def test_without_appointment():
    data = "mohit12"
    response = client.get(f'/receptionist/patient_appointments/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text
    assert response.json() == "No appointment Found for this patient", response.text


def test_none():
    response = client.get(f'/receptionist/patient_appointments/',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter the patient name"}, response.text


def test_wrong_patient():
    data = "abhi12"
    response = client.get(f'/receptionist/patient_appointments/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No patient found with such name"}, response.text


def test_without_token():
    data = "abhi12"
    response = client.get(f'/receptionist/patient_appointments/?patient_name={data}')

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_with_appointment():
    book_appoint()
    data = "mohit12"
    response = client.get(f'/receptionist/patient_appointments/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text
