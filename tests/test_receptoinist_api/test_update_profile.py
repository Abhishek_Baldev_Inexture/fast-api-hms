import json
from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login

client = TestClient(app)

token = test_recep_login.test_recep_login()


def test_short_pass():
    data = {"password": "1234"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Length of password should be greater than 8"}, response.text


def test_wrong_email():
    data = {"email": "test@.com"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter valid E-mail"}, response.text


def test_wrong_contact():
    data = {"contact": "99897876"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Length of contact should not less than 10"}


def test_taken_user_name():
    data = {"user_name": "mohit12"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "User name already taken, Please Enter another name"}, response.text


def test_update_profile():
    # test_register.test_register()
    data = {"user_name": "riya123", "password": "@Jay1234"}
    response = client.patch('/update_profile', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text
    assert response.json() == "Details Updated successfully", response.text

