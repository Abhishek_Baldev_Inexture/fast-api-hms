import json
from fastapi.testclient import TestClient
from sqlalchemy import delete

import models
from main import app
from ..test_receptoinist_api import test_recep_login, test_add_doctor
from .. import create_database


client = TestClient(app)
token = test_recep_login.test_recep_login()


def book_appoint():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "16/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_with_appointment():
    book_appoint()
    data = "khushi12"
    response = client.get(f'/receptionist/doctor_appointments/?doctor_name={data}',
                          headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code == 200, response.text


def test_without_appoint():
    stmt = delete(models.Appointment)
    print(stmt)
    create_database.engine.execute(stmt)

    data = "khushi12"
    response = client.get(f'/receptionist/doctor_appointments/?doctor_name={data}',
                          headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No appointment Found for this doctor"}, response.text


def test_none():
    data = "khushi12"
    response = client.get(f'/receptionist/doctor_appointments/?doctor_name=',
                          headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter the doctor name"}, response.text


def test_wrong_name():
    data = "abhi12"
    response = client.get(f'/receptionist/doctor_appointments/?doctor_name={data}',
                          headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No Doctor name found, Please Enter a correct name"}, response.text


def test_without_token():
    data = "khushi12"
    response = client.get(f'/receptionist/doctor_appointments/?doctor_name={data}')

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text
