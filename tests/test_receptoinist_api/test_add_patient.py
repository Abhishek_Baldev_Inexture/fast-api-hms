import json
from fastapi.testclient import TestClient
from sqlalchemy import delete

import models
from main import app
from ..test_receptoinist_api import test_recep_login
from ..create_database import engine

client = TestClient(app)
token = test_recep_login.test_recep_login()


def test_add_patient():
    # test_register.test_register()

    data = {"user_name": "abhi12", "first_name": "abhi", "last_name": "baldev", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text
    assert response.json() == "Patient added successfully", response.text


def test_none():
    data = None
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_empty():
    data = {}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_missing_fields():
    data = {"user_name": "abhi12", "first_name": "abhi", "last_name": "baldev", "password": "@Jay1234",
            "contact": "9898989898"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Field 'email' is require for adding patient"}, response.text


def test_missing_values():
    data = {"user_name": "", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Field 'user_name' is require for adding patient"}, response.text


def test_short_password():
    data = {"user_name": "khushi123", "first_name": "khushi", "last_name": "patel", "password": "1234",
            "contact": "9898989898", "email": "test@gmail.com"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Length of password should be greater than 8"}, response.text


def test_valid_email():
    data = {"user_name": "khushi123", "first_name": "khushi", "last_name": "patel", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@.com"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter valid E-mail"}, response.text


def test_invalid_contact():
    data = {"user_name": "khushi123", "first_name": "khushi", "last_name": "patel", "password": "@Jay1234",
            "contact": "989898", "email": "test@gmai.com"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_without_token():
    data = {"user_name": "abhi12", "first_name": "abhi", "last_name": "baldev", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com"}
    response = client.post('/add_patient', json.dumps(data))

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_no_patient():
    stmt = delete(models.Role).where(models.Role.role_name == "patient")
    engine.execute(stmt)

    data = {"user_name": "abhi12", "first_name": "abhi", "last_name": "baldev", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com"}
    response = client.post('/add_patient', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No role name patient found"}, response.text

