from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login
from ..test_patient_Api import test_book_appointment

client = TestClient(app)

token = test_recep_login.test_recep_login()
test_book_appointment.test_book_appointment()


def test_confirm_appointment():
    app_id = 1
    response = client.put(f'/receptionist/confirm_appointments/{app_id}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text
    assert response.json() == "Appointment is confirm", response.text


def test_wrong_id():
    app_id = 2
    response = client.put(f'/receptionist/confirm_appointments/{app_id}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No appointments with id found"}, response.text


def test_already_confirm():
    app_id = 1
    response = client.put(f'/receptionist/confirm_appointments/{app_id}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Appointment already confirmed"}, response.text


def test_without_id():
    # app_id = 1
    response = client.put(f'/receptionist/confirm_appointments/',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not Found"}, response.text
