import json
from fastapi.testclient import TestClient
from sqlalchemy import delete

from main import app

import models
from ..test_receptoinist_api import test_recep_login, test_add_doctor
from ..create_database import engine

client = TestClient(app)

# test_register.test_register()
token = test_recep_login.test_recep_login()


def book_appoint():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "15/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})


def test_see_history():
    book_appoint()
    data = "mohit12"
    response = client.get(f'/appointment_history/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No appointment history of patient"}, response.text


def test_none():
    response = client.get(f'/appointment_history/?patient_name=',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter the patient name"}, response.text


def test_empty():
    data = ""
    response = client.get(f'/appointment_history/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter the patient name"}, response.text


def test_without_token():
    data = "abhi12"
    response = client.get(f'/appointment_history/?patient_name={data}')

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_other_name():
    data = "abhi12"
    response = client.get(f'/appointment_history/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please Enter a correct username"}, response.text


def test_no_patient():
    stmt = delete(models.Role).where(models.Role.role_name == "patient")
    engine.execute(stmt)

    data = "mohit12"
    response = client.get(f'/appointment_history/?patient_name={data}',
                          headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No role name patient found"}, response.text
