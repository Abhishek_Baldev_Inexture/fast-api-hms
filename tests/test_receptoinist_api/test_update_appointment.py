import json
from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login, test_add_doctor

client = TestClient(app)
token = test_recep_login.test_recep_login()


def add_another_doctor():
    data = {"user_name": "vasu12", "first_name": "vasu", "last_name": "patel", "password": "@Jay1234",
            "contact": "9898989898", "email": "test@gmail.com", "specialist": "dentist"}
    response = client.post('/add_doctor', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})


def book_appoint():
    test_add_doctor.test_add_doctor()
    data = {"disease": "cavities", "app_date": "16/06/2022", "doctor_user_name": "khushi12",
            "patient_user_name": "mohit12"}

    response = client.post('/receptionist/book_appointment', json.dumps(data),
                           headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})


def test_update_app():
    # test_register.test_register()
    book_appoint()
    add_another_doctor()

    data = {"app_date": "17/6/2022", "doctor": "vasu12"}
    app_id = 1
    response = client.patch(f'/update_appointment/{app_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code == 200, response.text
    assert response.json() == "Appointment updated successfully", response.text


def test_wrong_date():
    data = {"app_date": "4/6/2022", "doctor": "vasu12"}
    app_id = 1
    response = client.patch(f'/update_appointment/{app_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please Enter a correct date"}, response.text


def test_wrong_doctor():
    data = {"app_date": "17/6/2022", "doctor": "nirav12"}
    app_id = 1
    response = client.patch(f'/update_appointment/{app_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No Doctor found, Please enter a correct name"}, response.text


def test_missing_value():
    data = {"app_date": "", "doctor": "vasu12"}
    app_id = 1
    response = client.patch(f'/update_appointment/{app_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text


def test_without_id():
    data = {"app_date": "17/6/2022", "doctor": "vasu12"}
    app_id = 1
    response = client.patch(f'/update_appointment', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})

    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not Found"}, response.text