import json
from fastapi.testclient import TestClient

from main import app
from ..test_receptoinist_api import test_recep_login, test_add_doctor

client = TestClient(app)

token = test_recep_login.test_recep_login()


def test_update_doctor():
    test_add_doctor.test_add_doctor()
    data = {"specialist": "orthopaedic"}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_without_token():
    data = {"specialist": "orthopaedic"}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data))
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_without_body():
    data = {"specialist": "orthopaedic"}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}',
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_none():
    data = None
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_empty():
    data = {}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_missing_values():
    data = {"specialist": ""}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text


def test_wrong_specialist():
    data = {"specialist": "ENT surgeon"}
    doctor_id = 3
    response = client.patch(f'/update_doctor/{doctor_id}', json.dumps(data),
                            headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "No Speciality found, Please enter a correct details"}, response.text
