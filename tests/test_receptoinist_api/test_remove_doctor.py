from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login, test_add_doctor

client = TestClient(app)

token = test_recep_login.test_recep_login()


def test_remove_doctor():
    test_add_doctor.test_add_doctor()
    doctor_id = 3
    response = client.delete(f'/delete_doctor/{doctor_id}',
                             headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code == 200, response.text


def test_wrong_id():
    test_add_doctor.test_add_doctor()
    doctor_id = 1
    response = client.delete(f'/delete_doctor/{doctor_id}',
                             headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Please enter a valid id for doctor"}, response.text


def test_without_id():
    """no need to call add doctor function because from above test_wrong_id function doctor is not deleted,
     so it will throw error of user already exist"""
    # test_add_doctor.test_add_doctor()
    doctor_id = 1
    response = client.delete('/delete_doctor',
                             headers={"Authorization": "Bearer {}".format(token["Jwt-token"])})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not Found"}, response.text


def test_without_token():
    doctor_id = 3
    response = client.delete(f'/delete_doctor/{doctor_id}')
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text

