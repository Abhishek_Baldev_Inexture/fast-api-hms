from fastapi.testclient import TestClient
from main import app
from ..test_receptoinist_api import test_recep_login

client = TestClient(app)

token = test_recep_login.test_recep_login()


def test_view_profile():
    response = client.get('/profile', headers={"Authorization": "Bearer {}".format(token['Jwt-token'])})
    assert response.status_code == 200, response.text


def test_without_token():
    response = client.get('/profile')
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Not authenticated"}, response.text


def test_wrong_token():
    response = client.get('/profile', headers={"Authorization": "Bearer ajsnasdkASKAskaJSUAshASKMASLKAdjdj"})
    assert response.status_code != 200, response.text
    assert response.json() == {"detail": "Could not validate credentials"}, response.text
