from fastapi import Depends, status, HTTPException
import token_2
from fastapi.security import OAuth2PasswordBearer

#  this tokenurl says from where we have to fetch the token i.e. from login
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


def get_current_user(data: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    return token_2.verify_token(data, credentials_exception)
