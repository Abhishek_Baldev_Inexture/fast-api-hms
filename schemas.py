# from datetime import datetime
from pydantic import BaseModel
from typing import Optional


class TokenData(BaseModel):
    username: Optional[str] = None


class Login(BaseModel):
    user_name: Optional[str] = None
    password: Optional[str] = None


class User(BaseModel):
    username: str


class Profile(BaseModel):
    user_name: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    password: Optional[str] = None
    contact: Optional[str] = None
    email: Optional[str] = None


class Add_doctor(Profile):
    specialist: Optional[str] = None


class Book_appointment(BaseModel):
    disease: Optional[str] = None
    app_date: Optional[str] = None
    doctor_user_name: Optional[str] = None


class Receptionist_book_appointment(Book_appointment):
    patient_user_name: Optional[str] = None


class See_appointment(BaseModel):
    doctor_name: str
    date: str
    disease: str


class Update_appointment(BaseModel):
    app_date: Optional[str] = None
    doctor: Optional[str] = None


class Specialist(BaseModel):
    specialist: Optional[str] = None


class Add_role(BaseModel):
    role_name: Optional[str] = None
