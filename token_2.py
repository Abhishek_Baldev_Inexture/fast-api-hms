from datetime import datetime, timedelta
from jose import JWTError, jwt
import schemas
import os
from dotenv import load_dotenv


SECRET_KEY = os.getenv('TOKEN_SECRET_KEY')
ALGORITHM = os.getenv('TOKEN_ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES = 180


def create_access_token(data: dict):
    to_encode = data.copy()

    # if there is expire time then add it else create expire time for 15 min

    # if expires_delta:
    #     expire = datetime.utcnow() + expires_delta
    # else:

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        user_name: str = payload.get("sub")
        if user_name is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=user_name)
        return token_data

    except JWTError:
        raise credentials_exception
