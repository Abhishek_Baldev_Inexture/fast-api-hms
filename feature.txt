Mainliy Three roles:
	1.Receptionist
	2.Doctor
	3.Patient
	
Receptionist:

- can view his/her profile.
- add doctor
- remove doctor
- update doctor details
- book an appointment


Patient:
- can see hi/her profile.
- can update profile.
- see the appointment
- see the list of doctors

Doctor:
- can see hi/her profile.
- can update profile.
- see the appointment
- treat people and give medicine
